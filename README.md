<h3>Vagner Lucas Gomes</h3>
<p>Viale Alcide de Gasperi 8</p>
<p>Treviglio BG, 24047</p>
<p>Italy</p>
<p>Mobile: +39 3284107661</p>
<p>E-Mail: vagner.lucas.gomes@gmail.com</p>
<p>26 years old,		Nationality: Brazilian</p>
<a>linkedin.com/in/vagner-lucas-gomes-52975766	</a> 
<h>
<h3>Summary</h3>
<p>Skilled software developer with more than 5 years of experience in designing, developing and integrating software. In-depth knowledge of Java and its APIs (Rest, Hibernate, iReport, etc), libraries and framewoks, HTML5, JavaScript, jQuery. Basic knowledge of Android, Python, Golang, C ++ and C-</p>
<h3>Skills</h3>
<p>Java Specialist/JEE;</p>
<p>In-depth knowledge of HTML5</p><p>JavaScripit</p><p> Jquery</p><p> Google Maps API</p>
<p>In-depth knowledge of Java, J2EE,SE, JavaWeb, J2EE, Servlet, RestFul/Soap, I-report, Hibernate;</p>
<p>Advanced troubleshooting capabilities;</p>
<p>Data Management-MySQL, Oracle Data Base</p>
<p>Knowledge of C, C++, C programming to AVR microcontroller</p>
<p>API design knowledge;</p>
<p>Exceptional time management;</p>
<p>Android programming knowledge;</p>
<p>Linux knowledge</p>
<p>Go language knowledge (Google);</p>
<p>Python knowledge;</p>
<p>Django knowledge;</p>

<h3> Education and training</h3>
<h4> Current</h4>
<p> University of West Santa Catarina – UNOESCt</p>
<p> Master degree of a scientific nature: Computer Engineering</p>

<h3> Certification/Courses</h3>
<h4> Additional information</h4> 
<p>•<a href='http://www.devmedia.com.br/' target='_blank' > Courses DevMedia:</a></p>
<p>• Full application using Hibernate - 2015</p>
<p>• IReport Course - Develop with Java Reporting - 2015</p>
<p>• Java Web: Servlet, JSP, JSTL e tag - 2015</p>
<p>• Developing a complete application with JPA- 2015</p>
<p>• Specifications Cases of Use- 2015</p>
<p>• iReport - Creating Java with reports for web applications - 2015</p>
<p>• Image manipulation with Java- 2015</p>
<p>• Java EE Course: Build a complete Java EE application - </p>2015</p>
<p>• Use Case Diagram in practice - 2015</p>
<p>• A virtual store creation course with Android Studio - ANDROID - 2016</p>
<h3> Professional experiences</h3>
<h4> Value Plus S.R.L</h4>
<p> January 2017 - Current</p>
<p> Java Developer</p>
<p> Summary of skills and activities:</p>
<p> 1-Development of new features for insurance policy issuing system, integration with various services (rest/soap), bug fixing and system maintenance.
Java EE, Rest, Soap, JBoss 7.1 HTML, JavaScript, Jquery, Bootstrap</p>
<h4> Beije Consulting</h4>
<p> December 2016 - December 2017</p>
<p> Java Developer</p>
<p> Summary of skills and activities:</p>
<p> 1-consulting programmer.
Java EE, Rest, Soap, JBoss 7.1 HTML, JavaScript, Jquery, Bootstrap</p>
<h4>HabitatWeb  Catanduvas -SC/Brasile</h4>
<p> July 2012－Giugno 2016</p>
<p> Developer/Analyst</p>
<p> Summary of skills and activities:</p>
<h4><p> 1 - LicençaWeb is a system for environmental licenses </ p></h4>
<p>• Function: Analysis, Requirements Survey, logical project database, system modeling, back-end programming, front-end programming, tests.</p> 
<p>The technologies used in the project.</p>
<p>• Java EE 7, Hibernate, MySQL, JasperReport I-Report, Restfull (webservice), servlet Java Server Pages, HTML5, CSS3, Bootstrap, JavaScritp, API di Google Maps, jQuery.</p>
<p>• Server: Apache Tomcat 7/8</p>
<p>• Scrum</p>
<p>• Project time: 1 year.</p>
<h4><p>2 - AgroLactus, a system for monitoring milk collection.</p></h4>
<p>• Function: Analysis, Requirements Survey, logical project database, system modeling, back-end programming, front-end programming, tests.</p>
<p> The technologies used in the project.</p>
<p>• Java EE 7, Hibernate, MySQL, JasperReport I-Report, Restfull (webservice), servlet Java Server Pages, HTML5, CSS3, Bootstrap, JavaScritp, API di Google Maps, jQuery.</p>
<p>• Server: Apache Tomcat 7/8</p>
<p>• Project times: 1 year and 4 months.</p>
<p>• Scrum </p>
<h4><p>3-Geofrotas is a distribution fleet management system with rural feed.</p></h4>
<p>• Function: Analysis, Requirements Survey, logical project database, system modeling, back-end programming, front-end programming, tests</p>
<p> The technologies used in the project.</p>
<p>• Java EE 7, Hibernate, MySQL, JasperReport I-Report, Restfull (webservice), servlet, Java Server Pages, HTML5, CSS3, <p>Bootstrap, JavaScritp, API di Google Maps, jQuery.</p>
<p>• Server: Apache Tomcat 7</p>
<p>• Project times: 1 year and 6 months.</p>
<p>• Scrum</p>
<h4><p>4 - GEOCARE is a private security control system.</p></h4>
<p>• Function: Analysis, Requirements Survey, logical project database, system modeling, back-end programming, front-end programming, tests</p>
<p> The technologies used in the project.</p>
<p>• Java EE 7, Hibernate, MySQL, JasperReport I-Report, Restfull (webservice), servlet Java Server Pages, HTML5, CSS3, Bootstrap, JavaScritp, API di Google Maps, jQuery, App Android, prese di corrente.</p>
<p>• Server: Apache Tomcat 7</p>
<p>• Project times: 6 months.</p>
<p>• Scrum</p>

<h4>Languages</h4>
<p>• Native language - Portuguese</p>
<table  >
  <tr>
    <th>Language</th>
    <th>Skill</th>
  </tr>
  <tr>
    <td>Italian</td>
    <td>C1</td>
  </tr>
   <tr>
    <td>English</td>
    <td>B1</td>
  </tr>
</table>
